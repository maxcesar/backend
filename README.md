## Sobre o sistema
O sistema deve ser pensado para gerenciar os funcionários de uma empresa pequena. 

- Deve permitir cadastrar, buscar, atualizar e deletar um funcionário, tudo utilizando padrão REST.
- É permitido utilizar qualquer dependência adicional.
- O código do projeto deve estar disponível em um repositório publico GIT de sua escolha.
- O prazo para a conclusão do desafio é de **uma semana**.

**As etapas extras são opcionais e não é necessário fazer todas.**

## 1ª Etapa
Criar um projeto java utilizando Spring Boot.

Adicionar dependência do Spring Data, recomendado usar o H2 para esse desafio.

O projeto deve conter uma separação de três camadas.

## 2ª Etapa
Modelar o funcionário, deve conter os seguintes atributos: 

- id
- nome **Obrigatorio**
- sexo
- idade **Obrigatorio**
- cep **Obrigatorio**
- endereço
- bairro
- cidade
- estado

## 3ª Etapa
Criar os seguintes endpoints.
Todas os endpoints devem receber e responder no formato JSON. 

- POST - Cadastrar um usuário
- GET - Buscar um usuário específico pelo id dele
- GET - Buscar todos os usuários cadastrados
- GET - Buscar usuários por CEP
- PUT - Atualizar a entidade funcionário 
- DELETE - Excluir funcionário

## 4ª Etapa
Criar testes para validar todos os métodos.

## 5ª Etapa (Extra)
Quando informado apenas o CEP na criação do funcionário, o sistema deve buscar o endereço e setar os atributos correspondentes.
Utilizar a seguinte API https://viacep.com.br/ para consulta de CEP.

## 6ª Etapa (Extra)
Adicionar um endpoint utilizando o método PATCH para atualizar apenas os atributos informados do funcionário.
Lembrando que se apenas o CEP for alterado o mesmo deve consultar o endereço novamente.

## 7ª Etapa (Extra)
Subir o seu projeto em uma CLOUD de sua escolha.
